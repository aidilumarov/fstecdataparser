﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSTECDataParser.Models
{
    public class Threat
    {
        #region Members 
        private string _id;
        private string _name;
        private string _description;
        private string _source;
        private string _target;
        private bool _isConfidentialityRisk;
        private bool _isIntegrityRisk;
        private bool _isAccessibilityRisk;
        #endregion

        #region Properties
        /// <summary>
        /// Threat ID
        /// </summary>
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// Threat name
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Threat description
        /// </summary>
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// Possible sources of the threat
        /// </summary>
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        /// <summary>
        /// What this threat targets
        /// </summary>
        public string Target
        {
            get { return _target; }
            set { _target = value; }
        }

        /// <summary>
        /// Does this threat bring any risks to confidentiality
        /// </summary>
        public bool IsConfidentialityRisk
        {
            get { return _isConfidentialityRisk; }
            set { _isConfidentialityRisk = value; }
        }

        /// <summary>
        /// Does this threat bring any risks to integrity
        /// </summary>
        public bool IsIntegrityRisk
        {
            get { return _isIntegrityRisk; }
            set { _isIntegrityRisk = value; }
        }

        /// <summary>
        /// Does this threat bring any risks to accessibility
        /// </summary>
        public bool IsAccessibilityRisk
        {
            get { return _isAccessibilityRisk; }
            set { _isAccessibilityRisk = value; }
        }
        #endregion

    }
}