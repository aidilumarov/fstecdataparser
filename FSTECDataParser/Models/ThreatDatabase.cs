﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FSTECDataParser.Models
{
    public class ThreatDatabase
    {
        #region Members
        private static readonly string _downloadUrl = "https://bdu.fstec.ru/documents/files/thrlist.xlsx";
        private static readonly string _dbDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "db");
        private static readonly string _fileName = "current-db.xlsx";
        private static readonly string _tempFile = "temp-db.xlsx";
        #endregion

        #region Properties
        /// <summary>
        /// Url the database is going to be downloaded from
        /// </summary>
        public static string DownloadUrl
        {
            get { return _downloadUrl; }
        }

        /// <summary>
        /// Folder where the database resides
        /// </summary>
        public static string DbDirectory
        {
            get { return _dbDirectory; }
        }

        /// <summary>
        /// Name of the database file
        /// </summary>
        public static string FileName
        {
            get { return _fileName;}
        }

        /// <summary>
        /// Temporary file for update purposes
        /// </summary>
        public static string TempFile
        {
            get { return _tempFile; }
        }
        #endregion
    }
}
