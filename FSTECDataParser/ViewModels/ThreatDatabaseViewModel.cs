﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Net;
using System.Collections.ObjectModel;
using System.ComponentModel;
using FSTECDataParser.Models;
using OfficeOpenXml;
using System.Security.Cryptography;
using System.Windows.Input;
using FSTECDataParser.Library;

namespace FSTECDataParser.ViewModels
{
    public class ThreatDatabaseViewModel : ObservableObject
    {
        #region Members
        private ObservableCollection<ThreatViewModel> _threats = new ObservableCollection<ThreatViewModel>();
        #endregion

        #region Properties
        public ObservableCollection<ThreatViewModel> Threats
        {
            get { return _threats; }
            private set
            {
                if (_threats != value)
                {
                    _threats = value;
                    RaisePropertyChanged("Threats");
                }
            }
        }
        #endregion

        #region Construction
        public ThreatDatabaseViewModel()
        {
            PrepareLocalDb();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Checks if the database file exists
        /// </summary>
        /// <returns>true if exists</returns>
        public static bool DatabaseExists()
        {
            return File.Exists(Path.Combine(ThreatDatabase.DbDirectory, ThreatDatabase.FileName));
        }

        /// <summary>
        /// Creates a standard database directory in the application's root folder
        /// </summary>
        public static void CreateDatabaseDirectory()
        {
            if (!Directory.Exists(ThreatDatabase.DbDirectory)) Directory.CreateDirectory(ThreatDatabase.DbDirectory);
        }

        /// <summary>
        /// Downloads the database from the remote resource and places inside the database directory
        /// </summary>
        private static bool Download()
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.DownloadFile(ThreatDatabase.DownloadUrl, Path.Combine(ThreatDatabase.DbDirectory, ThreatDatabase.FileName));
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
            
        }

        private static void DownloadTemp()
        {
            using (var client = new WebClient())
            {
                client.DownloadFile(ThreatDatabase.DownloadUrl, Path.Combine(ThreatDatabase.DbDirectory, ThreatDatabase.TempFile));
            }
        }

        /// <summary>
        /// Utilizes local methods for initial database setup
        /// </summary>
        public void PrepareLocalDb()
        {
            CreateDatabaseDirectory();
            if(Download())
            {
                LoadEntitiesIntoCollection(Threats);
            }
            else
            {
                Threats.Add(new ThreatViewModel("Database has not yet been created. Check your Internet connection"));
            }
        }


        /// <summary>
        /// Parses data from the database into ThreatViewModel entities, and then places into ObservableCollection
        /// </summary>
        public void LoadEntitiesIntoCollection(ObservableCollection<ThreatViewModel> collection)
        {
            string dbfile = Path.Combine(ThreatDatabase.DbDirectory, ThreatDatabase.FileName);

            // Read the xlsx database in
            var fi = new FileInfo(dbfile);
            using (var p = new ExcelPackage(fi))
            {
                var ws = p.Workbook.Worksheets["Sheet"];

                // foreach row
                for (int row = 3; row <= ws.Dimension.Rows; row++)
                {
                    string id = String.Format("УБИ.{0}",ws.Cells[row, 1].Text.PadLeft(3, '0'));
                    string name = ws.Cells[row, 2].Text.Replace("_x000d_", "");
                    string description = ws.Cells[row, 3].Text.Replace("_x000d_", ""); ;
                    string source = ws.Cells[row, 4].Text.Replace("_x000d_", ""); ;
                    string target = ws.Cells[row, 5].Text.Replace("_x000d_", ""); ;
                    bool isConfidentialityRisk = ws.Cells[row, 6].Text == "1";
                    bool isIntegrityRisk = ws.Cells[row, 7].Text == "1";
                    bool isAccessibilityRisk = ws.Cells[row, 8].Text == "1";
                    collection.Add(new ThreatViewModel(id, name, description, source, target, isConfidentialityRisk, isIntegrityRisk, isAccessibilityRisk));
                    RaisePropertyChanged("Threats");
                }
            }
        }
        #endregion

        #region Commands
        /// <summary>
        /// Updates the threat database
        /// </summary>
        public void UpdateDbExecute()
        {
            byte[] tempHash;
            byte[] currentHash;

            if (File.Exists(Path.Combine(ThreatDatabase.DbDirectory, ThreatDatabase.FileName)))
                DownloadTemp();
            else
            {
                Download();
                CommandManager.InvalidateRequerySuggested();
                return;
            }

            // Calculate checksums and see if there is any difference
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(Path.Combine(ThreatDatabase.DbDirectory, ThreatDatabase.TempFile)))
                {
                    tempHash = md5.ComputeHash(stream);
                }

                using (var stream = File.OpenRead(Path.Combine(ThreatDatabase.DbDirectory, ThreatDatabase.FileName)))
                {
                    currentHash = md5.ComputeHash(stream);
                }
            }

            if (tempHash.ToString() == currentHash.ToString())
            {
                MessageBox.Show("Already up to date!");
                return;
            }

            ObservableCollection<ThreatViewModel> temp = new ObservableCollection<ThreatViewModel>();
            LoadEntitiesIntoCollection(temp);

            // Here will save what has changed
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < Threats.Count; i++)
            {
                if (Threats[i].ThreatId != temp[i].ThreatId) sb.AppendFormat("Previous version: {0}; New version: {1}\n", Threats[i].ThreatId, temp[i].ThreatId);
                if (Threats[i].ThreatName != temp[i].ThreatName) sb.AppendFormat("Previous version: {0}; New version: {1}\n", Threats[i].ThreatName, temp[i].ThreatName);
                if (Threats[i].ThreatDescription != temp[i].ThreatDescription) sb.AppendFormat("Previous version: {0}; New version: {1}\n", Threats[i].ThreatDescription, temp[i].ThreatDescription);
                if (Threats[i].ThreatSource != temp[i].ThreatSource) sb.AppendFormat("Previous version: {0}; New version: {1}\n", Threats[i].ThreatSource, temp[i].ThreatSource);
                if (Threats[i].ThreatTarget != temp[i].ThreatTarget) sb.AppendFormat("Previous version: {0}; New version: {1}\n", Threats[i].ThreatTarget, temp[i].ThreatTarget);
                if (Threats[i].IsThreatConfidentialityRisk != temp[i].IsThreatConfidentialityRisk) sb.AppendFormat("Previous version: {0}; New version: {1}\n", Threats[i].IsThreatConfidentialityRisk, temp[i].IsThreatConfidentialityRisk);
                if (Threats[i].IsThreatIntegrityRisk != temp[i].IsThreatIntegrityRisk) sb.AppendFormat("Previous version: {0}; New version: {1}\n", Threats[i].IsThreatIntegrityRisk, temp[i].IsThreatIntegrityRisk);
                if (Threats[i].IsThreatAccessibilityRisk != temp[i].IsThreatAccessibilityRisk) sb.AppendFormat("Previous version: {0}; New version: {1}\n", Threats[i].IsThreatAccessibilityRisk, temp[i].IsThreatAccessibilityRisk);
            }
            MessageBox.Show(sb.ToString());

            Threats = temp;
            File.Delete(Path.Combine(ThreatDatabase.DbDirectory, ThreatDatabase.FileName));
            File.Move(Path.Combine(ThreatDatabase.DbDirectory, ThreatDatabase.TempFile), Path.Combine(ThreatDatabase.DbDirectory, ThreatDatabase.FileName));
        }

        private bool CanUpdateDbExecute()
        {
            return true;
        }

        public ICommand UpdateDb { get { return new RelayCommand(UpdateDbExecute, CanUpdateDbExecute); } }
        #endregion
    }
}
