﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FSTECDataParser.Models;
using FSTECDataParser.Library;
using System.ComponentModel;

namespace FSTECDataParser.ViewModels
{
    public class ThreatViewModel : ObservableObject
    {
        #region Members
        private Threat _threat;
        #endregion

        #region Construction
        public ThreatViewModel(string id = "", string name = "", string description = "",
                               string source = "", string target = "", bool isConfidentialityRisk = false,
                               bool isIntegrityRisk = false, bool isAccessibilityRisk = false)
        {
            _threat = new Threat()
            {
                Id = id,
                Name = name,
                Description = description,
                Source = source,
                Target = target,
                IsConfidentialityRisk = isConfidentialityRisk,
                IsIntegrityRisk = isIntegrityRisk,
                IsAccessibilityRisk = isAccessibilityRisk
            };
        }
        #endregion

        #region Properties
        public Threat Threat
        {
            get { return _threat; }
            set { _threat = value; }
        }

        public string ThreatId
        {
            get { return Threat.Id; }
            set
            {
                if (Threat.Id != value)
                {
                    Threat.Id = value;
                    RaisePropertyChanged("ThreatId");
                }
            }
        }

        public string ThreatName
        {
            get { return Threat.Name; }
            set
            {
                if (Threat.Name != value)
                {
                    Threat.Name = value;
                    RaisePropertyChanged("ThreatName");
                }
            }
        }

        public string ThreatDescription
        {
            get { return Threat.Description; }
            set
            {
                if (Threat.Description != value)
                {
                    Threat.Description = value;
                    RaisePropertyChanged("ThreatDescription");
                }
            }
        }

        public string ThreatSource
        {
            get { return Threat.Source; }
            set
            {
                if (Threat.Source != value)
                {
                    Threat.Source = value;
                    RaisePropertyChanged("ThreatSource");
                }
            }
        }

        public string ThreatTarget
        {
            get { return Threat.Target; }
            set
            {
                if (Threat.Target != value)
                {
                    Threat.Target = value;
                    RaisePropertyChanged("ThreatTarget");
                }
            }
        }

        public bool IsThreatConfidentialityRisk
        {
            get { return Threat.IsConfidentialityRisk; }
            set
            {
                if (Threat.IsConfidentialityRisk != value)
                {
                    Threat.IsConfidentialityRisk = value;
                    RaisePropertyChanged("IsThreatConfidentialityRisk");
                }
            }
        }

        public bool IsThreatIntegrityRisk
        {
            get { return Threat.IsIntegrityRisk; }
            set
            {
                if (Threat.IsIntegrityRisk != value)
                {
                    Threat.IsIntegrityRisk = value;
                    RaisePropertyChanged("IsThreatIntegrityRisk");
                }
            }
        }

        public bool IsThreatAccessibilityRisk
        {
            get { return Threat.IsAccessibilityRisk; }
            set
            {
                if (Threat.IsAccessibilityRisk != value)
                {
                    Threat.IsAccessibilityRisk = value;
                    RaisePropertyChanged("IsThreatAccessibilityRisk");
                }
            }
        }
        #endregion
    }
}
